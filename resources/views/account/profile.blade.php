@extends('layouts.app')

@section('styles')

@endsection

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">

                <div class="col s12 m3">
                    <div class="agent-sidebar">
                        NO SIDEBAR
                    </div>
                </div>

                <div class="col s12 m9">
                    <div class="agent-content">
                        <h4 class="agent-title">PROFILE</h4>

                        <form action="/profileupdate" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="row">
                                <div class="input-field col s6">
                                    <i class="material-icons prefix">person</i>
                                    <input id="name" name="name" type="text" value="{{ $usr->name }}" class="validate">
                                    <label for="name">Name</label>
                                </div>
                                <div class="input-field col s6">
                                    <i class="material-icons prefix">assignment_ind</i>
                                    <input id="username" name="username" type="text" value="{{ $usr->username or null }}" class="validate">
                                    <label for="username">Username</label>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="input-field col s4">
                                    <i class="material-icons prefix">email</i>
                                    <input id="email" name="email" type="email" value="{{ $usr->email }}" class="validate">
                                    <label for="email">Email</label>
                                </div>
                                <div class="file-field input-field col s6">
                                    <div class="btn indigo">
                                        <span>Image</span>
                                        <input type="file" name="image">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                                <div class="file-field input-field col s2">
                                    @if(Storage::disk('public')->exists('users/'.$usr->image) && $usr->image )
                                        <img src="{{Storage::url('users/'.$usr->image)}}" alt="{{$usr->title}}" class="responsive-img">
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">mode_edit</i>
                                    <textarea id="about" name="about" class="materialize-textarea">{{ $usr->about or null }}</textarea>
                                    <label for="about">About</label>
                                </div>
                            </div>

                            <div class="row">
                                <button class="btn waves-effect waves-light btn-large indigo darken-4" type="submit">
                                    Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>

                        </form>


                    </div>
                </div> <!-- /.col -->

            </div>
        </div>
    </section>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('textarea#about').characterCounter();
    });
</script>
@endsection