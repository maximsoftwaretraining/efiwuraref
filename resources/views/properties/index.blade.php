@extends('layouts.app')

@section('styles')
@endsection

@section('content')

    <section class="section">
        <div class="container">
            <div class="row">

                <div class="col s12 m3">
                    <div class="agent-sidebar">
                        SIDEBAR GOES HERE
                    </div>
                </div>

                <div class="col s12 m9">

                    <h4 class="agent-title">PROPERTY LIST</h4>
                    
                    <div class="agent-content">
                        <table class="striped responsive-table">
                            <thead>
                                <tr>
                                    <th>SL.</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>City</th>
                                    <th><i class="material-icons small-star p-t-10">comment</i></th>
                                    <th><i class="material-icons small-star p-t-10">stars</i></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                    
                            <tbody>
                                @foreach( $prps as $key => $prp )
                                    <tr>
                                        <td class="right-align">{{$key+1}}.</td>
                                        <td>
                                            <span class="tooltipped" data-position="bottom" data-tooltip="{{$prp->title}}">
                                                {{ Str::limit($prp->title, 30) }}
                                            </span>
                                        </td>
                                        
                                        <td>{{ ucfirst($prp->type) }}</td>
                                        <td>{{ ucfirst($prp->city) }}</td>

                                        <td class="center">
                                            <span><i class="material-icons small-comment left">comment</i>{{ $prp->comments_count }}</span>
                                        </td>

                                        <td class="center">
                                            @if($prp->featured == true)
                                                <span class="indigo-text"><i class="material-icons small-star">stars</i></span>
                                            @endif
                                        </td>
    
                                        <td class="center">
                                            <a href="properties/{{ $prp->id }}" target="_blank" class="btn btn-small green waves-effect">
                                                <i class="material-icons">visibility</i>
                                            </a>
                                            <a href="properties/{{ $prp->id }}/edit" class="btn btn-small orange waves-effect">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <button type="button" class="btn btn-small deep-orange accent-3 waves-effect" onclick="deleteProperty({{$prp->id}})">
                                                <i class="material-icons">delete</i>
                                            </button>
                                            <form action="properties/{{ $prp->id }}" method="POST" id="del-property-{{$prp->id}}" style="display:none;">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="center">
                            Render links to following pages
                        </div>
                    </div>
        
                </div>

            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        function deleteProperty(id){
            swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            buttons: true,
            dangerMode: true,
            buttons: ["Cancel", "Yes, delete it!"]
            }).then((value) => {
                if (value) {
                    document.getElementById('del-property-'+id).submit();
                    swal(
                    'Deleted!',
                    'Property has been deleted.',
                    'success',
                    {
                        buttons: false,
                        timer: 1000,
                    })
                }
            })
        }
    </script>
@endsection