<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PropertyImage;
use Faker\Generator as Faker;

$factory->define(PropertyImage::class, function (Faker $faker) {
    return [
    	'path' => $faker->slug,
    	'thumbnail_path' => $faker->slug,
    	'width' => $faker->randomDigit,
    	'height' => $faker->randomDigit,
    ];
});
	