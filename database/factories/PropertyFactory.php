<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Property;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Property::class, function (Faker $faker) {
    return [
    	'user_id' => factory(User::class),
    	'title' => $faker->catchPhrase,
    	'price' => $faker->randomFloat(2, 0, 1000000),
   		'purpose' => 'sale',
   		'type' => 'house',
    	'bedroom' => $faker->randomDigit,
    	'bathroom' => $faker->randomDigit,
   		'city' => $faker->city,
   		'address' => $faker->address,
   		'area' => '0',
    	'description' => $faker->realText(100),
   		'location_latitude' => $faker->latitude,
   		'location_longitude' => $faker->longitude,
   		'nearby' => $faker->realText(20),
    ];
});
	
	
	
	