<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsignedBigInteger();
            $table->string('path');
            $table->string('thumbnail_path');
            $table->integer('width');
            $table->integer('height');
            $table->timestamps();
            
            $table->index('property_id');
            // BUG foreign key gives General error: 1215 Cannot add foreign key constraint
            //$table->foreign('property_id')->references('id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_images');
    }
}
