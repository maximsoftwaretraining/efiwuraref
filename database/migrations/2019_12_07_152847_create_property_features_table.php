<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_features', function (Blueprint $table) {
            $table->bigInteger('property_id')->unsignedBigInteger();
            $table->bigInteger('feature_id')->unsignedBigInteger();
            
            $table->primary(['property_id', 'feature_id']);
    
            // BUG foreign key gives General error: 1215 Cannot add foreign key constraint
            //$table->foreign('property_id')->references('id')->on('properties');
            //$table->foreign('feature_id')->references('id')->on('features');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_features');
    }
}
