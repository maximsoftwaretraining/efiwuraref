<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('property_id')->unsignedBigInteger();
            $table->timestamp('date_from')->nullable();
            $table->timestamp('date_to')->nullable();
            $table->boolean('hidden')->default(false);
            $table->timestamps();
            
            $table->index('property_id');
            // BUG foreign key gives General error: 1215 Cannot add foreign key constraint
            //$table->foreign('property_id')->references('id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
