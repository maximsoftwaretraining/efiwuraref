<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('commentable_id');
            $table->string('commentable_type');
            $table->integer('parent')->default(0);
            $table->bigInteger('parent_id')->nullable();
            $table->text('body');
            $table->timestamps();
            
            $table->index('user_id');
            $table->index('commentable_id');
            $table->index('commentable_type');
            
            // BUG foreign key gives General error: 1215 Cannot add foreign key constraint
            //$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
