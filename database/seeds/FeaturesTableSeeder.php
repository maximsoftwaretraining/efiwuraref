<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeaturesTableSeeder extends Seeder {
	public function run() {
		DB::table('features')->insert([	'name' => 'garage', 'slug' => 'garage',]);
		DB::table('features')->insert([	'name' => 'swimmingpool', 'slug' => 'swimmingpool',]);
	}
}
