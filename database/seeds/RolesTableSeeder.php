<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder {

	public function run() {
	    DB::table('roles')->    insert([
	        [
	            'id'            => 1,
	            'name'          => 'admin',
	        ],
	        [
	            'id'            => 2,
	            'name'          => 'seller'
	        ],
	        [
	            'id'            => 3,
	            'name'          => 'member'
	        ]
	    ]);
	}
}
