<?php
use App\Models\Property;
use App\Models\PropertyImage;
use Illuminate\Database\Seeder;

class PropertiesTableSeeder extends Seeder {

	public function run() {
		factory(Property::class, 3)
			->create()
			->each(function ($prp) {
				$prp->propertyImages()->createMany(factory(PropertyImage::class, 2)->make()->toArray());
			});
	}
}
