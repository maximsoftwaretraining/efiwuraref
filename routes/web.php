<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/search', 'HomeController@search');
Route::get('/property/{id}', 'PropertyController@show');

Route::group(['middleware'=>['auth','member']], function(){
    Route::get('/profile', 'AccountController@profile');
    Route::post('profileupdate','AccountController@profileupdate');
    Route::resource('properties', 'PropertyController');
});

Auth::routes();
