<?php

namespace Tests\Unit\Models;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp(): void {
		parent::setUp();
	}

    public function testAllTest()
    {
    	$users = User::all();
    	$this->assertEquals(0, sizeof($users));
    }
}
