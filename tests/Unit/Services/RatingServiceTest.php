<?php
namespace Tests\Unit\Services;

use App\Models\Property;
use App\Models\User;
use App\Services\RatingService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RatingServiceTest extends TestCase
{
    use RefreshDatabase;
    
    protected $rs;
	
	public function setUp(): void {
		parent::setUp();
		$this->seed('UsersTableSeeder');
		$this->seed('PropertiesTableSeeder');
		$this->rs = new RatingService;
	}
	
	public function testSaveNew() {
	    $usr = User::all()->first();
	    $prp = Property::all()->first();
	    
		$untypedKeysArr = [
		    'user_id' => $usr->id,
		    'property_id' => $prp->id,
		    'type' => 'property',
		];

		$this->rs->save($untypedKeysArr, ['rating' => 4]);
		$this->assertEquals(1, sizeof($prp->ratings));
	}
}
