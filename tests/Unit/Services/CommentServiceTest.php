<?php
namespace Tests\Unit\Services;

use App\Models\Property;
use App\Models\User;
use App\Services\CommentService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CommentServiceTest extends TestCase
{
    use RefreshDatabase;
    
    protected $cs;
	
	public function setUp(): void {
		parent::setUp();
		$this->seed('UsersTableSeeder');
		$this->seed('PropertiesTableSeeder');
		$this->cs = new CommentService;
	}
	
	public function testSaveNew() {
	    $usr = User::all()->first();
	    $prp = Property::all()->first();
		    
	    $untypedArr = [
	        'user_id' => $usr->id,
	        'body' => 'This is my new commment',
	    ];
	    
	    $coms = $this->cs->findAllByUser($usr->id);
	    $com = $this->cs->saveNew($untypedArr, $prp);
	    $this->assertEquals('This is my new commment', $com->body);
	    $this->assertEquals(sizeof($coms) + 1, sizeof($usr->comments));
	}

	public function testSaveNewChild() {
	    $usr = User::all()->first();
	    $prp = Property::all()->first();
	    
	    $untypedArr = [
	        'user_id' => $usr->id,
	        'body' => 'This is my new commment',
	    ];
	    
	    $com = $this->cs->saveNew($untypedArr, $prp);
	    $this->assertEquals('This is my new commment', $com->body);
	    $this->assertEquals(1, sizeof($usr->comments));
	    
	    $untypedArrChild = [
	        'user_id' => $usr->id,
	        'body' => 'A commment on a comment',
	        'parent' => 1,
	        'parent_id' => $com->id,
	    ];
	    
	    $comc = $this->cs->saveNew($untypedArrChild, $prp);
	    $prp->refresh();
	    $this->assertEquals('A commment on a comment', $comc->body);
	    $this->assertEquals(2, sizeof($prp->comments));
	    
	    $coms = $this->cs->findAllByUser($usr->id);
	    $this->assertEquals(2, sizeof($coms));
	    
	}
}
