<?php
namespace Tests\Unit\Services;

use App\Models\User;
use App\Services\FeatureService;
use App\Services\PropertyService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PropertyServiceTest extends TestCase
{
	use RefreshDatabase;
	
	protected $ps;
	protected $fs;
	
	public function setUp(): void {
		parent::setUp();
		$this->seed('PropertiesTableSeeder');
		$this->ps = new PropertyService;
		$this->fs = new FeatureService;
	}
	
	public function testFirstByType() {
		$prp = $this->ps->findByType('house');
		$this->assertNotNull($prp);
		$this->assertNotNull($prp->user);
		$this->assertEquals(2, sizeof($prp->propertyImages));

		foreach ($prp->propertyImages as $pim) {
			echo $pim->path."\n";
		}
		
		// alternative to loop above, but now using a closure
		$prp->propertyImages->each(function ($pim) {
			echo $pim->path."\n";
		});
	}
	
	public function testSearch() {
	    $untypedArr = [
	        'minprice' => null,
	        'maxprice' => null,
	        'purpose' => null,
	        'type' => null,
	        'bedroom' => null,
	        'bathroom' => null,
	        'city' => null,
	        'minarea' => null,
	        'maxarea' => null,
	        'featured' => null,
	    ];
	    $prps = $this->ps->search($untypedArr);
	    $this->assertEquals(3, sizeof($prps));
	}
	    
	public function testFindNotFound() {
		$this->expectException(ModelNotFoundException::class);
		$this->expectExceptionMessage("No query results for model [App\Models\Property] 9999");
		
		$this->ps->find(9999);
	}

	public function testSaveNew() {
	    $this->seed('UsersTableSeeder');
	    $this->seed('FeaturesTableSeeder');
	    
		$usr = User::all()->first(); 
		
		$untypedArr = [
			'user_id' => $usr->id,
			'title' => 'Beautiful house',
			'slug' => 'something23',
			'price' => 300.000,
			'purpose' => 'sale',
			'type' => 'house',
			'bedroom' => 4,
			'bathroom' => 3,
			'city' => 'Accra',
			'city_slug' => 'acc',
			'address' => 'Cantonments',
			'area' => 0,
			'description' => 'Very very big',
			'location_latitude' => '12.12345',
			'location_longitude' => '14.345666',
			'nearby' => 'very',
		];
		$fea = $this->fs->findByName('garage');

		$prp = $this->ps->saveNew($untypedArr, array($fea->id), 'imagename');
		$this->assertEquals('Beautiful house', $prp->title);
		
 		$untypedArrIm = [
			'property_id' => $prp->id,
			'path' => 'to_image',
			'thumbnail_path' => 'hereisathumbnail',
			'width' => 640,
			'height' => 320,
		];
		$pim = $this->ps->saveNewPropertyImage($untypedArrIm, $prp);
		$this->assertEquals(320, $pim->height);
		
		$untypedArrIm['id'] = $pim->id;
		$untypedArrIm['width'] = 700;
		$pim = $this->ps->updatePropertyImage($untypedArrIm, $prp);
		$this->assertEquals(700, $pim->width);
		
		$pim->width = 900;
		$pim->save();
		$this->assertEquals(900, $pim->width);
		
		$this->assertEquals(2, sizeof($prp->propertyImages));
		
		echo $prp->propertyImages[0]->width;
	}
}
