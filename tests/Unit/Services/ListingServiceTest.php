<?php
namespace Tests\Unit\Services;

use App\Models\Property;
use App\Services\ListingService;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ListingServiceTest extends TestCase
{
    use RefreshDatabase;
    
    protected $ls;
	
	public function setUp(): void {
		parent::setUp();
		$this->seed('PropertiesTableSeeder');
		$this->ls = new ListingService;
	}
	
	public function testFindAllActive() {
	    $lsts = $this->ls->findAllActive();
	    $this->assertEquals(0, sizeof($lsts));
	}
	
	public function testSaveNew() {
	    $prp = Property::all()->first();
	    $df = Carbon::create(2019, 12, 31, 0);
		$dt = Carbon::create(2020, 5, 30, 0);
		
		$untypedArr = [
		    'date_from' => $df,
		    'date_to' => $dt,
		    'hidden' => 0,
		];

		$lst = $this->ls->saveNew($untypedArr, $prp);
		$this->assertEquals($dt, $lst->date_to);
		$this->assertEquals(1, sizeof($prp->listings));
		
		$lsts = $this->ls->findAllActive();
		$this->assertEquals(1, sizeof($lsts));
	}
}
