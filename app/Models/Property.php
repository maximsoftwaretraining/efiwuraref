<?php

namespace App\Models;

use App\Comment;
use App\Rating;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
    	'title',    'price',        'featured',     'purpose',      'type',       
        'bedroom',      'bathroom',     'city',     'address',      'area',
        'description',  'video',        'floor_plan',   'location_latitude', 'location_longitude',   'nearby',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    
    public function features()
    {
    	return $this->belongsToMany(Feature::class, 'property_features');
    }

    public function propertyImages()
    {
    	return $this->hasMany(PropertyImage::class);
    }

    public function ratings()
    {
    	return $this->hasMany(Rating::class);
    }

    public function listings()
    {
        return $this->hasMany(Listing::class);
    }
    
    public function comments()
    {
    	return $this->morphMany(Comment::class, 'commentable');
    }
}
