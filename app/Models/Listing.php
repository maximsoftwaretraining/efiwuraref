<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
	protected $fillable = ['date_from','date_to', 'hidden'];
    
	public function property()
	{
	    return $this->belongsTo(Property::class);
	}
}
