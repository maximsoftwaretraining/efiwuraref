<?php

namespace App;

use App\Models\Property;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = ['user_id', 'property_id', 'rating', 'type'];

    public function property()
    {
    	return $this->belongsTo(Property::class);
    }
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
