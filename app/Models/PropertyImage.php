<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyImage extends Model
{
    protected $fillable = [
        'path',
        'thumbnail_path',
        'width',
        'height'
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}


