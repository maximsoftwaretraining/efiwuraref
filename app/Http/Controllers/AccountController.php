<?php
namespace App\Http\Controllers;

use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AccountController extends Controller
{
    public function profile()
    {
        $usr = Auth::user();
        
        return view('account.profile', compact('usr'));
    }
    
    public function profileupdate(Request $request, UserService $us)
    {
        $request->validate([
            'name'      => 'required',
            'username'  => 'required',
            'email'     => 'required|email',
            'image'     => 'image|mimes:jpeg,jpg,png',
            'about'     => 'max:250'
        ]);
        
        $usr = Auth::user();
        
        $image = $request->file('image');
        $slug  = Str::slug($request->name);
        
        if(isset($image)){
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-agent-'.Auth::id().'-'.$currentDate.'.'.$image->getClientOriginalExtension();
            
            if(!Storage::disk('public')->exists('users')){
                Storage::disk('public')->makeDirectory('users');
            }
            if(Storage::disk('public')->exists('users/'.$usr->image) && $usr->image != 'default.png' ){
                Storage::disk('public')->delete('users/'.$usr->image);
            }
            $userimage = Image::make($image)->save();
            Storage::disk('public')->put('users/'.$imagename, $userimage);
        } else {
            $imagename = null;
        }

        $us->update($usr, $request->all(), $imagename);
        
        toastr()->success('Profile updated successfully');
        return back();
    }
}
