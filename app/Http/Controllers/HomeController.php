<?php
namespace App\Http\Controllers;

use App\Services\PropertyService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(PropertyService $ps)
    {
        //$prps = $ps->findAll();
        //$sliders = [];
        
        toastr()->success('Just checking the message ...');
        
        return view('home.index');
    }

    public function search(Request $request, PropertyService $ps)
    {
        $prps = $ps->search($request->all());
        
        return view('home.search', compact('prps'));
    }
}
