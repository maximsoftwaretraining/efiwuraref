<?php
namespace App\Http\Controllers;

use App\Models\Property;
use App\Services\FeatureService;
use App\Services\PropertyService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PropertyController extends Controller
{
    public function index(PropertyService $ps)
    {
        $prps = $ps->findAll();
        
        return view('properties.index',compact('prps'));
    }

    public function show($id, PropertyService $ps)
    {
        $prp = $ps->find($id);
        
        return view('properties.show', compact('prp'));
    }
    
    public function create(PropertyService $ps, FeatureService $fs)
    {   
        $feas = $fs->findAll();

        return view('properties.create',compact('feas'));
    }

    public function edit($id, PropertyService $ps, FeatureService $fs)
    {
        $feas = $fs->findAll();
        $prp = $ps->find($id);

        return view('properties.edit',compact('prp','feas'));
    }
    
    public function store(Request $request, PropertyService $ps) 
    { 
        $this->validateProperty($request);
        
        $image = $request->file('image');
        $imagename = null;
        if (isset($image)) {
            $imagename = $this->imageToDisk($image, $request->title);
        }

        $ps->saveNew($request->all(), $request->features, $imagename);

        toastr()->success('Property created successfully');
        return redirect()->route('properties.index');
    }

    public function update($id, Request $request, PropertyService $ps)
    {   
        $this->validateProperty($request);
        
        $image = $request->file('image');
        $imagename = null;
        if (isset($image)) {
            $imagename = $this->imageToDisk($image, $request->title);
        }
        
        $ps->update($id, $request->all(), $request->features, $imagename);
        
        toastr()->success('Property updated successfully');
        return redirect()->route('properties.index');
    }

    public function destroy(Property $property)
    {
        $property = Property::find($property->id);

        if(Storage::disk('public')->exists('property/'.$property->image)){
            Storage::disk('public')->delete('property/'.$property->image);
        }
        if(Storage::disk('public')->exists('property/'.$property->floor_plan)){
            Storage::disk('public')->delete('property/'.$property->floor_plan);
        }

        $property->delete();
        $property->features()->detach();
        
        toastr()->success('Property deleted successfully');
        return back();
    }

    private function validateProperty(Request $request)
    {
        $request->validate([
            'title'     => 'required|max:255',
            'price'     => 'required',
            'purpose'   => 'required',
            'type'      => 'required',
            'bedroom'   => 'required',
            'bathroom'  => 'required',
            'city'      => 'required',
            'address'   => 'required',
            'area'      => 'required',
            'image'     => 'image|mimes:jpeg,jpg,png',
            'floor_plan'=> 'image|mimes:jpeg,jpg,png',
            'description'        => 'required',
            'location_latitude'  => 'required',
            'location_longitude' => 'required'
        ]);
    }
    
    private function imageToDisk($image, $title)
    {
        $currentDate = Carbon::now()->toDateString();
        $imagename = Str::slug($title).'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
        
        if(!Storage::disk('public')->exists('property')){
            dd($title);
            Storage::disk('public')->makeDirectory('property');
        }

        $propertyimage = Image::make($image)->save();
        Storage::disk('public')->put('property/'.$imagename, $propertyimage);
        
        return $imagename;
    }
}
