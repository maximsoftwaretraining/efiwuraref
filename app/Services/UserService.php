<?php
namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService {

	public function findAll() {
		return User::all();
	}

	public function find($id) {
		return User::findOrFail($id);
	}

	public function update($usr, array $untypedArr, $imagename)
	{
	    $usr->name = $untypedArr['name'];
	    $usr->email = $untypedArr['email'];
	    if (isset($imagename)) {
	        $usr->image = $imagename;
	    }
	    $usr->save();
	}
	
}