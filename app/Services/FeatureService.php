<?php
namespace App\Services;

use App\Models\Feature;

class FeatureService 
{
	public function findAll() {
		return Feature::all();
	}

	public function find($id) {
		return Feature::findOrFail($id);
	}

	public function findAllByName($nameArr) {
		return Feature::whereIn('name', $nameArr)->get();
	}
	
	public function findByName($name) {
		return Feature::where('name', $name)->first();
	}

	public function save($untypedArr) {
		$fea = new Feature();
		$fea->name = $untypedArr['name'];
		$fea->slug = $untypedArr['slug'];
		$fea->save();
	
		return $fea;
	}
}