<?php
namespace App\Services;

use App\Comment;
use App\Models\Property;

class CommentService
{
    public function findAll()
    {
        return Comment::all();
    }

    public function find($id)
    {
        return Comment::findOrFail($id);
    }

    public function findAllByUser($id)
    {
        return Comment::where('user_id', $id)->get();
    }
    
    public function findAllByProperty($id)
    {
        return Comment::where('commentable_id', $id)->where('commentable_type', 'App\Models\Property')->get();
    }
    
    public function saveNew($untypedArr, $prp)
    {
        $com = new Comment();
        return $this->saveComment($com, $untypedArr, $prp);
    }

    public function update($untypedArr, $prp)
    {
        $com = new Comment();
        $com->exists = true;
        return $this->saveComment($com, $untypedArr, $prp);
    }

    private function saveComment($com, $untypedArr, $prp)
    {
        $com->id = isset($untypedArr['id']) ? $untypedArr['id'] : null;
        $com->user_id = $untypedArr['user_id'];
        $com->body = $untypedArr['body'];
        $com->parent = isset($untypedArr['parent']) ? $untypedArr['parent'] : 0;
        $com->parent_id = isset($untypedArr['parent_id']) ? $untypedArr['parent_id'] : 0;
        $prp->comments()->save($com);

        // alternative for line above
        //$com->commentable()->associate($prp);
        //$com->save();
        
        return $com;
    }
}

