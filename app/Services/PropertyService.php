<?php
namespace App\Services;

use App\Models\Property;
use App\Models\PropertyImage;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PropertyService
{
    public function findAll()
    {
        return Property::all();
    }

    public function find($id)
    {
        return Property::findOrFail($id);
    }

    public function findAllByName($nameArr)
    {
        return Property::whereIn('name', $nameArr)->get();
    }

    public function findByType($type)
    {
        return Property::where('type', $type)->first();
    }

    public function search($untypedArr)
    {
        $city     = strtolower($untypedArr['city']);
        $type     = isset($untypedArr['type']) ? $untypedArr['type'] : null;
        $purpose  = isset($untypedArr['purpose']) ? $untypedArr['purpose'] : null;
        $bedroom  = isset($untypedArr['bedroom']) ? $untypedArr['bedroom'] : null;
        $bathroom = isset($untypedArr['bathroom']) ? $untypedArr['bathroom'] : null;
        $minprice = isset($untypedArr['minprice']) ? $untypedArr['minprice'] : null;
        $maxprice = isset($untypedArr['maxprice']) ? $untypedArr['maxprice'] : null;
        $minarea  = isset($untypedArr['minarea']) ? $untypedArr['minarea'] : null;
        $maxarea  = isset($untypedArr['maxarea']) ? $untypedArr['maxarea'] : null;
        $featured = isset($untypedArr['featured']) ? $untypedArr['featured'] : null;
        
        $prps = Property::latest()->get()
        ->when($city, function ($query, $city) {
            return $query->where('city', '=', $city);
        })
        ->when($type, function ($query, $type) {
            return $query->where('type', '=', $type);
        })
        ->when($purpose, function ($query, $purpose) {
            return $query->where('purpose', '=', $purpose);
        })
        ->when($bedroom, function ($query, $bedroom) {
            return $query->where('bedroom', '=', $bedroom);
        })
        ->when($bathroom, function ($query, $bathroom) {
            return $query->where('bathroom', '=', $bathroom);
        })
        ->when($minprice, function ($query, $minprice) {
            return $query->where('price', '>=', $minprice);
        })
        ->when($maxprice, function ($query, $maxprice) {
            return $query->where('price', '<=', $maxprice);
        })
        ->when($minarea, function ($query, $minarea) {
            return $query->where('area', '>=', $minarea);
        })
        ->when($maxarea, function ($query, $maxarea) {
            return $query->where('area', '<=', $maxarea);
        })
        ->when($featured, function ($query, $featured) {
            return $query->where('featured', '=', 1);
        });
        
        return $prps;
    }
    
    public function saveNew($untypedArr, $feaids = [], $imagename)
    {
        $prp = new Property();
        return $this->saveProperty($untypedArr, $prp, $feaids, $imagename);
        
        // alternative 
        //return Property::create($untypedArr);
    }

    public function update($id, $untypedArr, $feaids = [], $imagename)
    {
        $prp = new Property();
        $prp->id = $id;
        $prp->exists = true;
        return $this->saveProperty($untypedArr, $prp, $feaids, $imagename);
    }

    public function selectAndUpdate($untypedArr)
    {
        $id = isset($untypedArr['id']) ? $untypedArr['id'] : null;
        $prp = $this->find($id);
        return $this->saveProperty($untypedArr, $prp, null);
    }

    public function saveNewPropertyImage($untypedArr, $prp)
    {
        $pim = new PropertyImage();
        return $this->savePropertyImage($pim, $untypedArr, $prp);
    }

    public function updatePropertyImage($untypedArr, $prp)
    {
        $pim = new PropertyImage();
        $pim->exists = true;
        return $this->savePropertyImage($pim, $untypedArr, $prp);
    }

    private function saveProperty($untypedArr, $prp, $feaids, $imagename)
    {
        //$prp->id = isset($untypedArr['id']) ? $untypedArr['id'] : null;
        if (Auth::user() != null) {
            $prp->user()->associate(Auth::user());
        } else {
            $u = new User();
            $u->id = $untypedArr['user_id'];
            $prp->user()->associate($u);
        }
        $prp->title = $untypedArr['title'];
        $prp->price = $untypedArr['price'];
        $prp->purpose = $untypedArr['purpose'];
        $prp->type = $untypedArr['type'];
        $prp->bedroom = $untypedArr['bedroom'];
        $prp->bathroom = $untypedArr['bathroom'];
        $prp->city = $untypedArr['city'];
        $prp->address = $untypedArr['address'];
        $prp->area = $untypedArr['area'];
        $prp->description = $untypedArr['description'];
        $prp->location_latitude = $untypedArr['location_latitude'];
        $prp->location_longitude = $untypedArr['location_longitude'];
        $prp->nearby = $untypedArr['nearby'];
        $prp->save();

        if (isset($imagename)) {
            $prp->propertyImages()->create([
                'path' => $imagename,
                'thumbnail_path' => 'hereisathumbnail',
                'width' => 640,
                'height' => 320,
            ]);
        }
        
        if (!isset($feaids)) {
            $feaids = [];
        }
        $this->saveFeatures($prp, $feaids);

        return $prp;
    }

    private function savePropertyImage($pim, $untypedArr, $prp)
    {
        $pim->id = isset($untypedArr['id']) ? $untypedArr['id'] : null;
        $pim->path = $untypedArr['path'];
        $pim->thumbnail_path = $untypedArr['thumbnail_path'];
        $pim->width = $untypedArr['width'];
        $pim->height = $untypedArr['height'];
        $prp->propertyImages()->save($pim);

        return $pim;
    }

    private function saveFeatures($prp, $feaids)
    {
        $prp->features()->sync($feaids);
    }
}