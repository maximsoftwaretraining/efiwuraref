<?php
namespace App\Services;

use App\Models\Listing;
use Carbon\Carbon;

class ListingService
{
    public function findAll()
    {
        return Listing::all();
    }

    public function find($id)
    {
        return Listing::findOrFail($id);
    }

    public function findAllActive($date = null)
    {
        if ($date == null) {
            $date = Carbon::now();
        }

        return Listing::where('date_from', '<=', $date)->where('date_to', '>', $date)->get();
    }

    public function saveNew($untypedArr, $prp)
    {
        $lst = new Listing();
        return $this->saveListing($lst, $untypedArr, $prp);
    }

    public function update($untypedArr, $prp)
    {
        $lst = new Listing();
        $lst->exists = true;
        return $this->saveListing($lst, $untypedArr, $prp);
    }

    private function saveListing($lst, $untypedArr, $prp)
    {
        $lst->id = isset($untypedArr['id']) ? $untypedArr['id'] : null;
        $lst->date_from = $untypedArr['date_from'];
        $lst->date_to = $untypedArr['date_to'];
        $lst->hidden = $untypedArr['hidden'];
        $lst->property()->associate($prp);
        $lst->save();

        return $lst;
    }
}