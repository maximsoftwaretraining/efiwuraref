<?php
namespace App\Services;

use App\Rating;

class RatingService 
{
	public function findAll() {
		return Rating::all();
	}

	public function save($untypedKeysArr, $untypedValuesArr) {
	    return Rating::updateOrCreate($untypedKeysArr, $untypedValuesArr);
	}
}

